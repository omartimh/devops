import subprocess

libs = []
cmd = 'rm'
directory = 'build/'
with open('libs_list.txt', mode = 'r') as file:
    lines = file.readlines()
    for line in lines:
        lib = line.split('\n')[0]
        libs.append(lib)
# Run shell command to copy libs from source to destination.
for lib in libs:
    subprocess.Popen(f'{cmd} {directory}/{lib}', shell = True, stdout = subprocess.DEVNULL)