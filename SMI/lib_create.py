import os, subprocess

libs = []
cmd = 'cp'
source = 'libs'
destination = 'build/'
with open('libs_list.txt', mode = 'r') as file:
    lines = file.readlines()
    for line in lines:
        lib = line.split('\n')[0]
        libs.append(lib)
# Run shell command to copy libs from source to destination.
for lib in libs:
    with os.scandir(source) as directory:
        if lib in directory:
            subprocess.Popen(f'{cmd} {source}/{lib} {destination}', shell = True, stdout = subprocess.DEVNULL)